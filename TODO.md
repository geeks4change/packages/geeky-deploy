# Geeks-Deploy TODO.md
- Mache inline process output
- Setze vendor/bin path in runner
- Finde Lösung für "" quoting mit shell Variablen
- Mache appDeploymentCmd configurable
- Schalte um auf drush main:set https://www.drush.org/12.x/commands/maint_set/
- Mache maint:set off in shutdown handler
- Verschiebe maint:set in pre-/post-deploy
- Erstelle einen timestamp $deploymentIdentifier für settings
  - Pseudo-globale Variable? DeployInfo singleton oder neuer Parameter?
- Mache ein .deploy.env mit dev/production Variable
- builder::addDotEnvBuilder()? Oder einfach alles in settings und container parameters?
- Erstelle einen timestamp release identifier in file or .deploy.env

# Version 7 (if needed)
- NOT drush, to be universal
- Invoke with symfony cmd

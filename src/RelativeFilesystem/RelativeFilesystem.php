<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\RelativeFilesystem;

use Brick\VarExporter\VarExporter;
use Geeks4Change\GeekyDeploy\Utility\EnsureFileIsWritable;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;

final class RelativeFilesystem extends Filesystem {

  private readonly string $dir;
  private readonly Filesystem $decorated;

  public function __construct(
    string $dir,
    private readonly LoggerInterface $logger,
  ) {
    $this->dir = Path::makeAbsolute($dir, getcwd());
    if (!file_exists($this->dir)) {
      throw new \UnexpectedValueException("Dir must exist: $this->dir");
    }
    if (!is_dir($this->dir)) {
      throw new \UnexpectedValueException("Dir must be a dir: $this->dir");
    }

    // We must subclass Filesystem because there is no interface.
    // Otoh we must decorate to not log self-calls.
    $this->decorated = new Filesystem();
  }

  public function getCurrentDir(): string {
    return $this->dir;
  }

  private function inDir(string|array $paths): string|array {
    if (is_string($paths)) {
      $paths = Path::makeAbsolute($paths, $this->dir);
      if (Path::isRelative($paths)) {
        $paths = "$this->dir/$paths";
      }
    }
    else {
      foreach ($paths as &$path) {
        $path = Path::makeAbsolute($path, $this->dir);
      }
    }
    return $paths;
  }

  /**
   * @param string|iterable $paths
   * @param-out string|array $paths
   */
  private function iterateToArray(string|iterable &$paths): void {
    // iterable := array|\Traversable
    // Some traversables like generators can only be iterated once, so do it.
    if ($paths instanceof \Traversable) {
      $paths = iterator_to_array($paths);
    }
  }

  public function copy(string $originFile, string $targetFile, bool $overwriteNewerFiles = FALSE) {
    $this->logger->info(sprintf("fs->copy(originFile: %s, targetFile: %s, overwriteNewerFiles: %s)", VarExporter::export($originFile), VarExporter::export($targetFile), VarExporter::export($overwriteNewerFiles)));
    $this->decorated->copy($this->inDir($originFile), $this->inDir($targetFile), $overwriteNewerFiles);
  }

  public function mkdir(iterable|string $dirs, int $mode = 0777) {
    $this->logger->info(sprintf("fs->mkdir(dirs: %s, mode: %s)", VarExporter::export($dirs), self::formatOctal($mode)));
    $this->decorated->mkdir($this->inDir($dirs), $mode);
  }

  public function exists(iterable|string $files): bool {
    $this->iterateToArray($files);
    $result = $this->decorated->exists($this->inDir($files));
    $this->logger->info(sprintf("fs->exists(files: %s) = %s", VarExporter::export($files), VarExporter::export($result)));
    return $result;
  }

  public function touch(iterable|string $files, ?int $time = NULL, ?int $atime = NULL) {
    $this->iterateToArray($files);
    $this->decorated->touch($this->inDir($files), $time, $atime);
    $this->logger->info(sprintf("fs->touch(files: %s, time: %s, atime: %s)", VarExporter::export($files), VarExporter::export($time), VarExporter::export($atime)));
  }

  public function remove(iterable|string $files) {
    $this->iterateToArray($files);
    $this->logger->info(sprintf("fs->remove(files: %s)", VarExporter::export($files)));
    // Make everything writable first.
    foreach ((array) $files as $file) {
      $this->makeWritableRecursively($file);
      // Also make parent writable temporarily.
      if (file_exists($this->inDir($file))) {
        $handles[] = new EnsureFileIsWritable($this->inDir($file));
      }
    }
    $this->decorated->remove($this->inDir($files));
    unset($handles);
  }

  public function chmod(iterable|string $files, int $mode, int $umask = 0000, bool $recursive = FALSE) {
    $this->iterateToArray($files);
    $this->logger->info(sprintf("fs->chmod(files: %s, mode: %s, umask: %s, recursive: %s)", VarExporter::export($files), self::formatOctal($mode), self::formatOctal($umask), VarExporter::export($recursive)));
    $this->decorated->chmod($this->inDir($files), $mode, $umask, $recursive);
  }

  public function chown(iterable|string $files, int|string $user, bool $recursive = FALSE) {
    $this->iterateToArray($files);
    $this->logger->info(sprintf("fs->chown(files: %s, user: %s, recursive: %s)", VarExporter::export($files), VarExporter::export($user), VarExporter::export($recursive)));
    $this->decorated->chown($this->inDir($files), $user, $recursive);
  }

  public function chgrp(iterable|string $files, int|string $group, bool $recursive = FALSE) {
    $this->iterateToArray($files);
    $this->logger->info(sprintf("fs->chgrp(files: %s, group: %s, recursive. %s)", VarExporter::export($files), VarExporter::export($group), VarExporter::export($recursive)));
    $this->decorated->chgrp($this->inDir($files), $group, $recursive);
  }

  public function rename(string $origin, string $target, bool $overwrite = FALSE) {
    $this->logger->info(sprintf("fs->rename(origin: %s, target: %s, overwrite: %s)", VarExporter::export($origin), VarExporter::export($target), VarExporter::export($overwrite)));
    $this->decorated->rename($this->inDir($origin), $this->inDir($target), $overwrite);
  }

  public function symlink(string $originDir, string $targetDir, bool $copyOnWindows = FALSE) {
    $this->logger->info(sprintf("fs->symlink(originDir: %s, targetDir: %s, copyOnWindows: %s)", VarExporter::export($originDir), VarExporter::export($targetDir), VarExporter::export($copyOnWindows)));
    // Must not make absolute $originDir, which is the link target.
    $this->decorated->symlink($originDir, $this->inDir($targetDir), $copyOnWindows);
  }

  public function hardlink(string $originFile, iterable|string $targetFiles) {
    $this->iterateToArray($targetFiles);
    $this->logger->info(sprintf("fs->hardlink(originFile: %s, targtFiles: %s)", VarExporter::export($originFile), VarExporter::export($targetFiles)));
    $this->decorated->hardlink($this->inDir($originFile), $this->inDir($targetFiles));
  }

  public function readlink(string $path, bool $canonicalize = FALSE): ?string {
    $result = $this->decorated->readlink($this->inDir($path), $canonicalize);
    $this->logger->info(sprintf("fs->readlink(path: %s, canonicalize: %s) = %s", VarExporter::export($path), VarExporter::export($canonicalize), VarExporter::export($result)));
    return $result;
  }

  public function mirror(string $originDir, string $targetDir, ?\Traversable $iterator = NULL, array $options = []) {
    $this->iterateToArray($iterator);
    $this->logger->info(sprintf("fs->mirror(originDir: %s, targetDir: %s, iterator: %s, options: %s)", VarExporter::export($originDir), VarExporter::export($targetDir), VarExporter::export($iterator), VarExporter::export($options)));
    $this->decorated->mirror($this->inDir($originDir), $this->inDir($targetDir), $iterator, $options);
  }

  public function tempnam(string $dir, string $prefix, string $suffix = ''): string {
    $this->logger->info(sprintf("fs->tempnam(dir: %s, prefix: %s, suffix: %s)", VarExporter::export($dir), VarExporter::export($prefix), VarExporter::export($suffix)));
    return $this->decorated->tempnam($this->inDir($dir), $prefix, $suffix);
  }

  public function dumpFile(string $filename, $content) {
    $this->logger->info(sprintf("fs->dumpFile(filename: %s, content: ...(%sb)", VarExporter::export($filename), strlen($content)));
    $this->decorated->dumpFile($this->inDir($filename), $content);
  }

  public function appendToFile(string $filename, $content) {
    $this->logger->info(sprintf("fs->dumpFile(filename: %s, content: ...(%sb)", VarExporter::export($filename), strlen($content)));
    $this->decorated->appendToFile($this->inDir($filename), $content);
  }

  // Added API functions.

  public function makeWritableAndGetHandle(string $path): EnsureFileIsWritable {
    $this->logger->info(sprintf("fs->makeWritable(path: %s)", VarExporter::export($path)));
    return new EnsureFileIsWritable($this->inDir($path));
  }

  public function isDir(string $path): bool {
    $result = is_dir($this->inDir($path));
    $this->logger->info(sprintf("fs->isDir(path: %s) = %s", VarExporter::export($path), VarExporter::export($result)));
    return $result;

  }

  public function isFile(string $path): bool {
    $result = is_file($this->inDir($path));
    $this->logger->info(sprintf("fs->isFile(path: %s) = %s", VarExporter::export($path), VarExporter::export($result)));
    return $result;

  }

  public function isLink(string $path): bool {
    $result = is_link($this->inDir($path));
    $this->logger->info(sprintf("fs->isLink(path: %s) = %s", VarExporter::export($path), VarExporter::export($result)));
    return $result;
  }

  // Internal helpers.

  private static function formatOctal(int $mode): string {
    return '0o' . decoct($mode);
  }

  private function makeWritableRecursively(string $filename): void {
    $filename = $this->inDir($filename);
    if (is_dir($filename)) {
      $filesystemIterator = new \FilesystemIterator($filename, \FilesystemIterator::CURRENT_AS_PATHNAME | \FilesystemIterator::SKIP_DOTS);
      foreach ($filesystemIterator as $child) {
        $this->makeWritableRecursively($child);
      }
    }
    if (file_exists($filename) && !is_writable($filename)) {
      chmod($filename, 0o700);
    }
  }

}

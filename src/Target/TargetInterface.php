<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Target;

use Geeks4Change\GeekyDeploy\Builder\BuilderInterface;

interface TargetInterface {

  public function getName(): string;

  public function getBuilder(): BuilderInterface;

  public function getTargets(): Targets;

  public function isDev(): bool;

  public function setTargets(Targets $targets): void;

  public function getUrl(): string;

}

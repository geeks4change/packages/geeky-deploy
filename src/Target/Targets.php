<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Target;

use Geeks4Change\GeekyDeploy\Target\Local\LocalTargetInterface;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTargetInterface;

final class Targets {

  private bool $frozen = FALSE;

  /**
   * @param array<string, \Geeks4Change\GeekyDeploy\Target\Local\LocalTargetInterface> $localTargets
   * @param array<string, \Geeks4Change\GeekyDeploy\Target\Remote\RemoteTargetInterface> $remoteTargets
   */
  private function __construct(
    protected array $localTargets,
    protected array $remoteTargets,
  ) {}

  public static function create(
  ): self {
    return new self([], []);
  }

  public function freeze(): self {
    $this->frozen = TRUE;
    return $this;
  }

  public function addLocal(LocalTargetInterface $target): void {
    if ($this->frozen) {
      throw new \LogicException('Can not add to frozen targets.');
    }
    $name = $target->getName();
    if (isset($this->localTargets[$name]) || isset($this->remoteTargets[$name])) {
      throw new \LogicException("Can not add target name twice: $name");
    }
    $target->setTargets($this);
    $this->localTargets[$name] = $target;
  }

  public function addRemote(RemoteTargetInterface $target): void {
    if ($this->frozen) {
      throw new \LogicException('Can not add to frozen targets.');
    }
    $name = $target->getName();
    if (isset($this->localTargets[$name]) || isset($this->remoteTargets[$name])) {
      throw new \LogicException("Can not add target name twice: $name");
    }
    $target->setTargets($this);
    $this->remoteTargets[$name] = $target;
  }

  public function getLocalNames(): array {
    if (!$this->frozen) {
      throw new \LogicException('Can not retrieve pre-frozen targets.');
    }
    return array_keys($this->localTargets);
  }

  public function getRemoteNames(): array {
    if (!$this->frozen) {
      throw new \LogicException('Can not retrieve pre-frozen targets.');
    }
    return array_keys($this->remoteTargets);
  }

  public function getLocal(?string $name): LocalTargetInterface {
    if (!$this->frozen) {
      throw new \LogicException('Can not retrieve pre-frozen targets.');
    }
    if (!$name) {
      if (count($this->localTargets) === 1) {
        return reset($this->localTargets);
      }
      else {
        throw new \OutOfBoundsException(sprintf('Can not retrieve unique local target. Options are: %s', implode(', ', $this->getLocalNames())));
      }
    }
    return $this->localTargets[$name]
      ?? throw new \OutOfBoundsException(sprintf('Unknown target name: %s. Options are: %s', $name, implode('|', $this->getLocalNames())));
  }

  public function getRemote(?string $name): RemoteTargetInterface {
    if (!$this->frozen) {
      throw new \LogicException('Can not retrieve pre-frozen targets.');
    }
    if (!$name) {
      if (count($this->remoteTargets) === 1) {
        return reset($this->remoteTargets);
      }
      else {
        throw new \OutOfBoundsException(sprintf('Can not retrieve unique remote target. Options are: %s', implode(', ', $this->getRemoteNames())));
      }
    }
    return $this->remoteTargets[$name]
      ?? throw new \OutOfBoundsException(sprintf('Unknown target name: %s. Options are: %s', $name, implode('|', $this->getRemoteNames())));
  }

}

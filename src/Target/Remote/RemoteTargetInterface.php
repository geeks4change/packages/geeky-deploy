<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Target\Remote;

use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Geeks4Change\GeekyDeploy\Runner\Remote\Ssh\SshRunnerFactoryInterface;
use Geeks4Change\GeekyDeploy\Server\ServerInterface;
use Geeks4Change\GeekyDeploy\Target\TargetInterface;

interface RemoteTargetInterface extends TargetInterface {

  public function deploy(
    LocalRunnerInterface $localRunner,
    SshRunnerFactoryInterface $remoteRunnerFactory,
    $suppressRelease = FALSE,
    $keepSandbox = FALSE,
  ): void;

  public function getServer(): ServerInterface;

  public function getUrl(): string;

}

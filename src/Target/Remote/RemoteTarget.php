<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Target\Remote;

use Geeks4Change\GeekyDeploy\Builder\BuilderInterface;
use Geeks4Change\GeekyDeploy\Runner\Local\LocalRunner;
use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Geeks4Change\GeekyDeploy\Runner\Remote\Ssh\SshRunnerFactoryInterface;
use Geeks4Change\GeekyDeploy\Runner\RunnerInterface;
use Geeks4Change\GeekyDeploy\Server\ServerInterface;
use Geeks4Change\GeekyDeploy\Target\TargetBase;
use Geeks4Change\GeekyDeploy\Utility\MkTemp;
use Geeks4Change\GeekyDeploy\Utility\OnShutdown\OnShutdown;
use Geeks4Change\GeekyDeploy\Utility\PhpUnitTool;

final class RemoteTarget extends TargetBase implements RemoteTargetInterface {

  protected function __construct(
    string $name,
    protected readonly string $url,
    protected readonly ServerInterface $server,
    BuilderInterface $builder,
  ) {
    parent::__construct($name, $builder);
  }

  public static function create(
    string $name,
    string $url,
    ServerInterface $server,
    BuilderInterface $builder,
  ): static {
    return new static($name, $url, $server, $builder);
  }

  final public function getUrl(): string {
    return $this->url;
  }

  /**
   * Command: deploy
   *
   * @api
   */
  final public function deploy(
    LocalRunnerInterface $localRunner,
    SshRunnerFactoryInterface $remoteRunnerFactory,
    $suppressRelease = FALSE,
    $keepSandbox = FALSE,
  ): void {
    // Prepare remote runner early, to spot errors.
    $sshInfo = $this->server->getSshInfo($this);
    $remoteRunner = $remoteRunnerFactory->createRunner($sshInfo);

    // Copy to tmp sandbox.
    $useDeterministicSandbox = PhpUnitTool::isRunningTest();
    $tmpSandboxDir = MkTemp::dir(
      prefix: 'geeky-deploy-',
      deterministic: $useDeterministicSandbox,
    );
    if (!$keepSandbox) {
      $dropSandbox = OnShutdown::create(
        fn() => $localRunner->getFilesystem()->remove($tmpSandboxDir)
      );
    }
    $this->rsyncToSandbox($localRunner, $tmpSandboxDir);
    $sandboxRunner = $localRunner->forWorkingDirectory($tmpSandboxDir);

    // @todo Let the builder drop the sites/default lock here, via a writerToken.

    // Allow server to modify before building.
    $this->server->beforeBuild($this, $sandboxRunner);

    // Build in sandbox.
    $this->getBuilder()->build($sandboxRunner, $this);

    // Copy sandbox to remote, and drop it.
    $this->server->releasePrepare($this, $sandboxRunner, $remoteRunner);

    // Release.
    $this->server->releaseSwitch($this, $sandboxRunner, $remoteRunner);
    if (!$suppressRelease) {
      $this->releaseInMaintenanceMode($remoteRunner);
    }
    $this->server->releaseUnlock($this, $sandboxRunner, $remoteRunner);

    // Drop sandbox not earlier, as the ssh process coughs if its cwd is gone.
    if (isset($dropSandbox)) {
      $dropSandbox->run();
    }
  }

  /**
   * @internal
   */
  final protected function rsyncToSandbox(LocalRunnerInterface $localRunner, string $sandboxPath) {
    $localRunner->mustRun(array_merge(
      ['rsync', '.', $sandboxPath],
      $this->getRsyncToSandboxExtraOptions(),
    ));
  }

  /**
   * @api May be overridden.
   */
  protected function getRsyncToSandboxExtraOptions(): array {
    // @todo Delegate sites/default/files exclude to builder.
    return ['-a', '--exclude', '.git/', '--exclude', '**/.git/', '--exclude', 'web/sites/default/files/**', '--exclude', 'private',];
  }

  /**
   * @api May be overridden.
   */
  protected function releaseInMaintenanceMode(RunnerInterface $runner): void {
    $this->releaseDbDump($runner);
    // @todo Set maintenance mode off on failure, like $tmpDir.
    // @fixme Replace with current command.
    $runner->run(['vendor/bin/drush', 'state:set', 'system.maintenance_mode', '1', '--input-format=integer']);
    $this->release($runner);
    $runner->run(['vendor/bin/drush', 'state:set', 'system.maintenance_mode', '0', '--input-format=integer']);
    $runner->run(['vendor/bin/drush', 'cache:rebuild']);
  }

  /**
   * @api May be overridden.
   */
  protected function releaseDbDump(RunnerInterface $runner): void {
    // Hardcode this until https://github.com/drush-ops/drush/issues/5675
    // and https://gitlab.com/geeks4change/packages/geeky-deploy/-/issues/10
    $runner->mustRun(array_merge(['vendor/bin/drush', 'sql:dump'], $this->getReleaseDumpExtraOptions()));
  }

  /**
   * @api May be overridden.
   */
  protected function getReleaseDumpExtraOptions(): array {
    return ['--result-file=auto', "--extra-dump=--no-tablespaces --column-statistics=0"];
  }

  /**
   * @api May be overridden.
   */
  protected function release(RunnerInterface $runner): void {
    $runner->mustRun(['vendor/bin/drush', 'deploy']);
    // If needed, implement CustomPostDeployCommands.
  }

  /**
   * @api May be overridden.
   */
  protected function drupalCanBootstrap(RunnerInterface $runner): bool {
    $process = $runner->run(['drush', 'status', '--field=bootstrap', '--format=string']);
    $bootstrapLevel = $process->isSuccessful() ? trim($process->getOutput()) : NULL;
    return boolval($bootstrapLevel);
  }

  final public function getServer(): ServerInterface {
    return $this->server;
  }

  /**
   * @api May be overridden.
   */
  public function isDev(): bool {
    return FALSE;
  }

}

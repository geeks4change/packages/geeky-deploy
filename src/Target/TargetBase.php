<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Target;

use Geeks4Change\GeekyDeploy\Builder\BuilderInterface;
use Geeks4Change\GeekyDeploy\Runner\RunnerInterface;

abstract class TargetBase implements TargetInterface {

  protected Targets $targets;

  protected function __construct(
    protected readonly string $name,
    protected readonly BuilderInterface $builder,
  ) {}

  final public function getName(): string {
    return $this->name;
  }

  final public function getBuilder(): BuilderInterface {
    return $this->builder;
  }

  final public function setTargets(Targets $targets): void {
    if (isset($this->targets)) {
      throw new \LogicException('Can not set targets twice.');
    }
    $this->targets = $targets;
  }

  final public function getTargets(): Targets {
    return $this->targets;
  }

  /**
   * Command: install
   */
  final public function install(RunnerInterface $runner, bool $reinstall = FALSE): void {
    $this->getBuilder()->install($runner, $reinstall);
  }

}

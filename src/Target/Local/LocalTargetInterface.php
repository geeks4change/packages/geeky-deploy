<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Target\Local;

use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Geeks4Change\GeekyDeploy\Target\TargetInterface;

interface LocalTargetInterface extends TargetInterface {

  public function build(LocalRunnerInterface $localRunner): void;

  public function serve(LocalRunnerInterface $localRunner): void;

}

<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Target\Local;

use Geeks4Change\GeekyDeploy\Builder\BuilderInterface;
use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Geeks4Change\GeekyDeploy\Target\TargetBase;

final class LocalTarget extends TargetBase implements LocalTargetInterface {

  private function __construct(
    string $name,
    protected readonly int $port,
    BuilderInterface $builder,
  ) {
    parent::__construct($name, $builder);
  }

  public static function create(
    string $name,
    int $port,
    BuilderInterface $builder,
  ): static {
    return new static($name, $port, $builder);
  }

  /**
   * @api May be overridden.
   */
  final public function getUrl(): string {
    return "http://localhost:$this->port";
  }

  /**
   * Command: serve
   */
  final public function serve(LocalRunnerInterface $localRunner): void {
    $this->getBuilder()->serve($localRunner, $this->port);
  }

  /**
   * Command: build
   */
  final public function build(LocalRunnerInterface $localRunner): void {
    $this->getBuilder()->build($localRunner, $this);
  }

  /**
   * @api May be overridden.
   */
  public function isDev(): bool {
    return TRUE;
  }

}

<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Utility;

final class MkTemp {

  public static function dir(?string $dir = NULL, string $prefix = 'tmp-', $mode=0700, bool $deterministic = FALSE): string {
    if (!$dir) {
      $dir = sys_get_temp_dir();
    }
    if (!is_dir($dir)) {
      throw new \UnexpectedValueException("Not a dir: $dir");
    }
    $dir = rtrim($dir, '/');
    $suffix = 0;
    do {
      if ($deterministic) {
        $suffix++;
      }
      else {
        $suffix = mt_rand(0, 99999999);
      }
      $path = "$dir/$prefix$suffix";
    } while (!mkdir($path, $mode));

    return $path;
  }

}

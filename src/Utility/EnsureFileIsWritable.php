<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Utility;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;
use function chmod;
use function file_exists;
use function fileperms;
use function is_writable;

/**
 * @internal
 */
final class EnsureFileIsWritable {

  private ?int $originalFilePerms = NULL;
  private ?int $originalDirPerms = NULL;
  private readonly string $absPath;

  /**
   * Make (maybe not yet existing) file and its directory writable temporarily.
   */
  public function __construct(
    string $path,
  ) {
    $this->absPath = Path::makeAbsolute($path, getcwd());
    $dir = dirname($this->absPath);

    $this->ensureDir($dir);
    assert(file_exists($dir));
    assert(is_dir($dir));

    if (!is_writable($dir)) {
      $this->originalDirPerms = fileperms($dir);
      $perms = $this->originalDirPerms | 0220; // ug+w
      chmod($dir, $perms);
    }
    if (file_exists($this->absPath) && !is_writable($this->absPath)) {
      $this->originalFilePerms = fileperms($this->absPath);
      $perms = $this->originalFilePerms | 0220; // ug+w
      chmod($this->absPath, $perms);
    }
  }

  private function ensureDir(string $dir): void {
    if (!file_exists($dir)) {
      $this->ensureDir(dirname($dir));
      mkdir($dir);
    }
  }

  public function __destruct() {
    $fs = new Filesystem();
    if (isset($this->originalFilePerms) && file_exists($this->absPath)) {
      $fs->chmod($this->absPath, $this->originalFilePerms);
    }
    if (isset($this->originalDirPerms) && file_exists(dirname($this->absPath))) {
      $fs->chmod(dirname($this->absPath), $this->originalDirPerms);
    }
  }


}

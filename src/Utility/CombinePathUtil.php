<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Utility;

final class CombinePathUtil {

  public static function combine(?string $basePath, ?string $path): ?string {
    if (isset($path) && isset($basePath)) {
      if (!str_starts_with($path, '/')) {
        return "$basePath/$path";
      }
      else {
        return $path;
      }
    }
    else {
      return $path ?? $basePath;
    }
  }

}

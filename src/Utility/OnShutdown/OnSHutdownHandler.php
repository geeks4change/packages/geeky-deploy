<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Utility\OnShutdown;

use Baraja\ShutdownTerminator\TerminatorHandler;

/**
 * @internal
 */
final class OnSHutdownHandler implements TerminatorHandler {

  public function __construct(
    private readonly OnShutdown $onShutdown,
  ) {}

  public function processTerminatorHandler(): void {
    $this->onShutdown->run();
  }

}
<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Utility\OnShutdown;

use Baraja\ShutdownTerminator\Terminator;

final class OnShutdown {

  private $status = OnShutdownStatus::pending;

  private function __construct(
    private readonly \Closure $closure,
  ) {}

  public static function create(\Closure $closure): self {
    $instance = new self($closure);
    Terminator::addHandler(new OnSHutdownHandler($instance));
    return $instance;
  }

  public function __destruct() {
    $this->run();
  }

  public function getStatus(): OnShutdownStatus {
    return $this->status;
  }

  public function run(): void {
    if ($this->status === OnShutdownStatus::pending) {
      ($this->closure)();
    }
    $this->status = OnShutdownStatus::hasRun;
  }

  public function abandon(): void {
    $this->status = OnShutdownStatus::abandoned;
  }

}

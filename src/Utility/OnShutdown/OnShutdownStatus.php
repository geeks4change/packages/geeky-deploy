<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Utility\OnShutdown;

enum OnShutdownStatus {

  case pending;
  case hasRun;
  case abandoned;

}

<?php

declare(strict_types=1);

namespace Geeks4Change\GeekyDeploy\Utility;

final class SshKnownHostFile {

  public static function add(array $pubKeysByHost): bool {
    if ($home = $_SERVER['HOME'] ?? NULL) {
      $sshDir = "$home/.ssh";
      $knownHostsFile = "$sshDir/known_hosts";
      if (is_dir($home) && !file_exists($sshDir)) {
        mkdir($sshDir);
      }
      if (is_dir($sshDir) && is_writable($sshDir)) {
        if (!file_exists($knownHostsFile)) {
          $knownHosts = [];
        }
        elseif (is_file($knownHostsFile) && is_readable($knownHostsFile) && is_writable($knownHostsFile)) {
          $knownHosts = self::readKnownHosts($knownHostsFile);
        }
        if (isset($knownHosts)) {
          // Overwrite values if needed.
          $knownHosts = array_merge($knownHosts, $pubKeysByHost);
          self::writeKnownHosts($knownHostsFile, $knownHosts);
          return true;
        }
      }
    }
    return false;
  }

  private static function readKnownHosts(string $knownHostsFile) {
    $lines = file($knownHostsFile);
    $knownHosts = [];
    foreach ($lines as $line) {

      if (strpos($line, '#') === 0) {
        // Add comments with integer key.
        $knownHosts[] = $line;
      }
      else {
        // Add other lines with string host key.
        [$host, $pubKey] = explode(' ', $line) + [1 => NULL];
        $knownHosts[$host] = $pubKey;
      }
    }
    return $knownHosts;
  }

  private static function writeKnownHosts(string $knownHostsFile, array $knownHosts) {
    $lines = [];
    foreach ($knownHosts as $host => $value) {
      if (is_int($host)) {
        $lines[] = $value;
      }
      else {
        $lines[] = "$host $value";
      }
    }
    $content = implode("\n", $lines);
    file_put_contents($knownHostsFile, $content);
  }

}

<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Utility;

final class PhpUnitTool {

  public static function isRunningTest(): bool {
    return defined('PHPUNIT_COMPOSER_INSTALL');
  }

}

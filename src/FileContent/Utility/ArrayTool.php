<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\FileContent\Utility;

final class ArrayTool {

  private mixed $value;

  private function __construct(
    mixed &$value,
  ) {
    $this->value =& $value;
  }

  public static function create(mixed &$value): self {
    return new self($value);
  }

  public function at(string ...$keys): self {
    if (!$keys) {
      return $this;
    }
    else {
      $key = array_shift($keys);
      return self::create($this->value[$key])
        ->at(...$keys);
    }
  }

  public function set(mixed $value): void {
    $this->value = $value;
  }

}

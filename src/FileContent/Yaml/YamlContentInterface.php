<?php

namespace Geeks4Change\GeekyDeploy\FileContent\Yaml;

use Geeks4Change\GeekyDeploy\FileContent\FileContentInterface;

interface YamlContentInterface extends FileContentInterface {

  public function getData(): mixed;

}
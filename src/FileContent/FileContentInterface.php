<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\FileContent;

interface FileContentInterface {

  public function getContent(): string;

}

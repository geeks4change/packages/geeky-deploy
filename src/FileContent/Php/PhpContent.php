<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\FileContent\Php;

final class PhpContent extends PhpContentBase {

  /**
   * @param array<int|string, string|PhpAssignment> $lines
   */
  private function __construct(
    private readonly array $lines,
  ) {}

  public static function create(string|PhpAssignment ...$lines): self {
    $constructorLines = [];
    foreach ($lines as $line) {
      self::addLine($constructorLines, $line);
    }
    return new self($lines);
  }

  /**
   * @return list<string|PhpAssignment>
   */
  public function getLines(): array {
    return array_values($this->lines);
  }

}

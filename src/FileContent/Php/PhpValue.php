<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\FileContent\Php;

use Brick\VarExporter\VarExporter;

final class PhpValue implements PhpValueInterface {

  private function __construct(
    private readonly mixed $value,
  ) {}

  public static function create(mixed $value): self {
    return new self($value);
  }

  public function __toString() {
    $export = VarExporter::export($this->value, VarExporter::TRAILING_COMMA_IN_ARRAY, 2);
    return $export;
  }

}

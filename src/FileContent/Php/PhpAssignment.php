<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\FileContent\Php;

final class PhpAssignment implements \Stringable {

  private function __construct(
    private readonly string $variable,
    private readonly \Stringable $value,
  ) {}

  public static function create(string $variable, mixed $value): self {
    if (!$value instanceof PhpValueInterface) {
      $phpValue = PhpValue::create($value);
    }
    return new self($variable, $phpValue);
  }

  public function getVariable(): string {
    return $this->variable;
  }

  public function __toString() {
    return "$this->variable = $this->value;";
  }


}

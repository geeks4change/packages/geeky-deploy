<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\FileContent\Php;

use Brick\VarExporter\VarExporter;

final class PhpLazyValue implements PhpValueInterface {

  private function __construct(
    private readonly \Closure $closure,
  ) {}

  public static function create(\Closure $closure): self {
    return new self($closure);
  }

  public function __toString() {
    $value = ($this->closure)();
    $export = VarExporter::export($value, VarExporter::TRAILING_COMMA_IN_ARRAY, 2);
    return substr($export, 2);
  }

}

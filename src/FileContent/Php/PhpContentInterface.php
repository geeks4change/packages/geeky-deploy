<?php

namespace Geeks4Change\GeekyDeploy\FileContent\Php;

use Geeks4Change\GeekyDeploy\FileContent\FileContentInterface;

interface PhpContentInterface extends FileContentInterface {

  /**
   * @return list<string|\Geeks4Change\GeekyDeploy\FileContent\Php\PhpAssignment>
   */
  public function getLines(): array;

}
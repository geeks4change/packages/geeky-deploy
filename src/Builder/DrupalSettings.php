<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Builder;

use Geeks4Change\GeekyDeploy\FileContent\Php\PhpAssignment;
use Geeks4Change\GeekyDeploy\FileContent\Php\PhpContentBase;

final class DrupalSettings extends PhpContentBase {

  /**
   * @param list<string> $fileScanIgnoreDirectories
   * @param list<string|PhpAssignment> $literalLines
   */
  private function __construct(
    public readonly ?string $hashSalt,
    public readonly ?string $configSyncDirectory = '../config/sync',
    public readonly array $trustedHostPatterns = [],
    public readonly array $database = [],
    public readonly ?string $isolationLevel = 'READ COMMITTED',
    public readonly ?string $filePrivatePath = NULL,
    public readonly ?string $filePrivateSharedDirectory = NULL,
    public readonly ?string $filePublicPath = NULL,
    public readonly ?string $filePublicSharedDirectory = NULL,
    public readonly ?string $fileTempPath = NULL,
    public readonly ?int $fileChmodDirectory = NULL,
    public readonly ?int $fileChmodFile = NULL,
    public readonly array $fileScanIgnoreDirectories = ['node_modules', 'bower_components',],
    public readonly array $literalLines = [],
  ) {}

  public static function create(
    string $hashSalt,
  ): self {
    return new self($hashSalt, '../config/sync', [], [], 'READ COMMITTED', NULL);
  }

  /**
   * @return list<string>
   */
  public function getSharedDirectories(): array {
    $candidates = [
      $this->filePublicSharedDirectory ?? 'sites/default/files',
      $this->filePrivateSharedDirectory,
    ];
    $directories = [];
    foreach ($candidates as $directory) {
      if (isset($directory)) {
        $directories[] = $directory;
      }
    }
    return $directories;
  }

  public function getLines(): array {
    $lines = [];
    if (isset($this->hashSalt)) {
      self::addLine($lines, PhpAssignment::create("\$settings['hash_salt']", $this->hashSalt));
    }
    if (isset($this->configSyncDirectory)) {
      self::addLine($lines, PhpAssignment::create("\$settings['config_sync_directory']", $this->configSyncDirectory));
    }
    if ($this->trustedHostPatterns) {
      self::addLine($lines, PhpAssignment::create("\$settings['trusted_host_patterns']", $this->trustedHostPatterns));
    }
    if ($this->database) {
      self::addLine($lines, PhpAssignment::create("\$databases['default']['default']", $this->database));
    }
    if (isset($this->filePrivatePath)) {
      self::addLine($lines, PhpAssignment::create("\$settings['file_private_path']", $this->filePrivatePath));
    }
    if (isset($this->filePublicPath)) {
      self::addLine($lines, PhpAssignment::create("\$settings['file_public_path']", $this->filePublicPath));
    }
    if (isset($this->fileTempPath)) {
      self::addLine($lines, PhpAssignment::create("\$settings['file_temp_path']", $this->fileTempPath));
    }
    if (isset($this->fileChmodDirectory)) {
      self::addLine($lines, PhpAssignment::create("\$settings['file_chmod_directory']", $this->fileChmodDirectory));
    }
    if (isset($this->fileChmodFile)) {
      self::addLine($lines, PhpAssignment::create("\$settings['file_chmod_file']", $this->fileChmodFile));
    }
    if ($this->fileScanIgnoreDirectories) {
      self::addLine($lines, PhpAssignment::create("\$settings['file_scan_ignore_directories']", $this->fileScanIgnoreDirectories));
    }

    if (isset($this->isolationLevel)) {
      // This code picks up DB settings also if prepended by server.
      $lines[] = "if ((\$databases['default']['default']['driver'] ?? NULL) === 'mysql') {";
      $lines[] = "  \$databases['default']['default']['init_commands']['isolation_level'] = 'SET SESSION TRANSACTION ISOLATION LEVEL $this->isolationLevel';";
      $lines[] = "}";
    }

    foreach ($this->literalLines as $literalLine) {
      self::addLine($lines, $literalLine);
    }

    return array_values($lines);
  }

  public function withConfigSyncDirectory(?string $configSyncDirectory): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $configSyncDirectory,
      trustedHostPatterns: $this->trustedHostPatterns,
      database: $this->database,
      isolationLevel: $this->isolationLevel,
      filePrivatePath: $this->filePrivatePath,
      filePrivateSharedDirectory: $this->filePrivateSharedDirectory,
      filePublicPath: $this->filePublicPath,
      filePublicSharedDirectory: $this->filePublicSharedDirectory,
      fileTempPath: $this->fileTempPath,
      fileChmodDirectory: $this->fileChmodDirectory,
      fileChmodFile: $this->fileChmodFile,
      fileScanIgnoreDirectories: $this->fileScanIgnoreDirectories,
      literalLines: $this->literalLines,
    );
  }

  public function withTrustedHostPatterns(string ...$trustedHostPatterns): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $this->configSyncDirectory,
      trustedHostPatterns: $trustedHostPatterns,
      database: $this->database,
      isolationLevel: $this->isolationLevel,
      filePrivatePath: $this->filePrivatePath,
      filePrivateSharedDirectory: $this->filePrivateSharedDirectory,
      filePublicPath: $this->filePublicPath,
      filePublicSharedDirectory: $this->filePublicSharedDirectory,
      fileTempPath: $this->fileTempPath,
      fileChmodDirectory: $this->fileChmodDirectory,
      fileChmodFile: $this->fileChmodFile,
      fileScanIgnoreDirectories: $this->fileScanIgnoreDirectories,
      literalLines: $this->literalLines,
    );
  }

  public function withTrustedHosts(string ...$trustedHosts): self {
    $trustedHostPatterns = array_map(
      fn($host) => sprintf('^%s$', preg_quote($host)),
      $trustedHosts
    );
    return $this->withTrustedHostPatterns(...$trustedHostPatterns);
  }

  public function withDatabase(array $database): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $this->configSyncDirectory,
      trustedHostPatterns: $this->trustedHostPatterns,
      database: $database,
      isolationLevel: $this->isolationLevel,
      filePrivatePath: $this->filePrivatePath,
      filePrivateSharedDirectory: $this->filePrivateSharedDirectory,
      filePublicPath: $this->filePublicPath,
      filePublicSharedDirectory: $this->filePublicSharedDirectory,
      fileTempPath: $this->fileTempPath,
      fileChmodDirectory: $this->fileChmodDirectory,
      fileChmodFile: $this->fileChmodFile,
      fileScanIgnoreDirectories: $this->fileScanIgnoreDirectories,
      literalLines: $this->literalLines,
    );
  }

  public function withIsolationLevel(?string $isolationLevel): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $this->configSyncDirectory,
      trustedHostPatterns: $this->trustedHostPatterns,
      database: $this->database,
      isolationLevel: $isolationLevel,
      filePrivatePath: $this->filePrivatePath,
      filePrivateSharedDirectory: $this->filePrivateSharedDirectory,
      filePublicPath: $this->filePublicPath,
      filePublicSharedDirectory: $this->filePublicSharedDirectory,
      fileTempPath: $this->fileTempPath,
      fileChmodDirectory: $this->fileChmodDirectory,
      fileChmodFile: $this->fileChmodFile,
      fileScanIgnoreDirectories: $this->fileScanIgnoreDirectories,
      literalLines: $this->literalLines,
    );
  }

  /**
   * @param string|null $filePrivateSharedDirectory
   *  The shared directory, if different,
   */
  public function withFilePrivatePath(?string $filePrivatePath, ?string $filePrivateSharedDirectory = NULL): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $this->configSyncDirectory,
      trustedHostPatterns: $this->trustedHostPatterns,
      database: $this->database,
      isolationLevel: $this->isolationLevel,
      filePrivatePath: $filePrivatePath,
      filePrivateSharedDirectory: $filePrivateSharedDirectory ?? $filePrivatePath,
      filePublicPath: $this->filePublicPath,
      filePublicSharedDirectory: $this->filePublicSharedDirectory,
      fileTempPath: $this->fileTempPath,
      fileChmodDirectory: $this->fileChmodDirectory,
      fileChmodFile: $this->fileChmodFile,
      fileScanIgnoreDirectories: $this->fileScanIgnoreDirectories,
      literalLines: $this->literalLines,
    );
  }

  /**
   * @param string|null $filePublicSharedDirectory
   *  The shared directory, if different,
   */
  public function withFilePublicPath(?string $filePublicPath, ?string $filePublicSharedDirectory = NULL): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $this->configSyncDirectory,
      trustedHostPatterns: $this->trustedHostPatterns,
      database: $this->database,
      isolationLevel: $this->isolationLevel,
      filePrivatePath: $this->filePrivatePath,
      filePrivateSharedDirectory: $this->filePrivateSharedDirectory,
      filePublicPath: $filePublicPath,
      filePublicSharedDirectory: $filePublicSharedDirectory ?? $filePublicPath,
      fileTempPath: $this->fileTempPath,
      fileChmodDirectory: $this->fileChmodDirectory,
      fileChmodFile: $this->fileChmodFile,
      fileScanIgnoreDirectories: $this->fileScanIgnoreDirectories,
      literalLines: $this->literalLines,
    );
  }

  public function withFileTempPath(?string $fileTempPath): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $this->configSyncDirectory,
      trustedHostPatterns: $this->trustedHostPatterns,
      database: $this->database,
      isolationLevel: $this->isolationLevel,
      filePrivatePath: $this->filePrivatePath,
      filePrivateSharedDirectory: $this->filePrivateSharedDirectory,
      filePublicPath: $this->filePublicPath,
      filePublicSharedDirectory: $this->filePublicSharedDirectory,
      fileTempPath: $fileTempPath,
      fileChmodDirectory: $this->fileChmodDirectory,
      fileChmodFile: $this->fileChmodFile,
      fileScanIgnoreDirectories: $this->fileScanIgnoreDirectories,
      literalLines: $this->literalLines,
    );
  }

  public function withFileChmodDirectory(?int $fileChmodDirectory): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $this->configSyncDirectory,
      trustedHostPatterns: $this->trustedHostPatterns,
      database: $this->database,
      isolationLevel: $this->isolationLevel,
      filePrivatePath: $this->filePrivatePath,
      filePrivateSharedDirectory: $this->filePrivateSharedDirectory,
      filePublicPath: $this->filePublicPath,
      filePublicSharedDirectory: $this->filePublicSharedDirectory,
      fileTempPath: $this->fileTempPath,
      fileChmodDirectory: $fileChmodDirectory,
      fileChmodFile: $this->fileChmodFile,
      fileScanIgnoreDirectories: $this->fileScanIgnoreDirectories,
      literalLines: $this->literalLines,
    );
  }

  public function withFileChmodFile(?int $fileChmodFile): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $this->configSyncDirectory,
      trustedHostPatterns: $this->trustedHostPatterns,
      database: $this->database,
      isolationLevel: $this->isolationLevel,
      filePrivatePath: $this->filePrivatePath,
      filePrivateSharedDirectory: $this->filePrivateSharedDirectory,
      filePublicPath: $this->filePublicPath,
      filePublicSharedDirectory: $this->filePublicSharedDirectory,
      fileTempPath: $this->fileTempPath,
      fileChmodDirectory: $this->fileChmodDirectory,
      fileChmodFile: $fileChmodFile,
      fileScanIgnoreDirectories: $this->fileScanIgnoreDirectories,
      literalLines: $this->literalLines,
    );
  }

  public function withFileScanIgnoreDirectories(array $fileScanIgnoreDirectories): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $this->configSyncDirectory,
      trustedHostPatterns: $this->trustedHostPatterns,
      database: $this->database,
      isolationLevel: $this->isolationLevel,
      filePrivatePath: $this->filePrivatePath,
      filePrivateSharedDirectory: $this->filePrivateSharedDirectory,
      filePublicPath: $this->filePublicPath,
      filePublicSharedDirectory: $this->filePublicSharedDirectory,
      fileTempPath: $this->fileTempPath,
      fileChmodDirectory: $this->fileChmodDirectory,
      fileChmodFile: $this->fileChmodFile,
      fileScanIgnoreDirectories: $fileScanIgnoreDirectories,
      literalLines: $this->literalLines,
    );
  }

  public function withPhpLine(string $line): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $this->configSyncDirectory,
      trustedHostPatterns: $this->trustedHostPatterns,
      database: $this->database,
      isolationLevel: $this->isolationLevel,
      filePrivatePath: $this->filePrivatePath,
      filePrivateSharedDirectory: $this->filePrivateSharedDirectory,
      filePublicPath: $this->filePublicPath,
      filePublicSharedDirectory: $this->filePublicSharedDirectory,
      fileTempPath: $this->fileTempPath,
      fileChmodDirectory: $this->fileChmodDirectory,
      fileChmodFile: $this->fileChmodFile,
      fileScanIgnoreDirectories: $this->fileScanIgnoreDirectories,
      literalLines: array_merge($this->literalLines, [$line]),
    );
  }

  public function withAssignment(PhpAssignment $assignment): self {
    return new self(
      hashSalt: $this->hashSalt,
      configSyncDirectory: $this->configSyncDirectory,
      trustedHostPatterns: $this->trustedHostPatterns,
      database: $this->database,
      isolationLevel: $this->isolationLevel,
      filePrivatePath: $this->filePrivatePath,
      filePrivateSharedDirectory: $this->filePrivateSharedDirectory,
      filePublicPath: $this->filePublicPath,
      filePublicSharedDirectory: $this->filePublicSharedDirectory,
      fileTempPath: $this->fileTempPath,
      fileChmodDirectory: $this->fileChmodDirectory,
      fileChmodFile: $this->fileChmodFile,
      fileScanIgnoreDirectories: $this->fileScanIgnoreDirectories,
      literalLines: array_merge($this->literalLines, [$assignment]),
    );
  }

  public function withConfigOverride(string $config, string $path, mixed $value): self {
    $variable = "\$config['$config']";
    foreach (explode('.', $path) as $key) {
      $variable .= "['$key']";
    }
    return $this->withAssignment(PhpAssignment::create($variable, $value));
  }

}

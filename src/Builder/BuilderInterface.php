<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Builder;

use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Geeks4Change\GeekyDeploy\Runner\RunnerInterface;
use Geeks4Change\GeekyDeploy\Server\ServerInterface;
use Geeks4Change\GeekyDeploy\Target\TargetInterface;

interface BuilderInterface {

  public function install(RunnerInterface $runner, bool $reinstall);

  public function build(LocalRunnerInterface $localRunner, TargetInterface $target): void;

  public function serve(LocalRunnerInterface $localRunner, int $port): void;

  /**
   *
   * @return list<string>
   */
  public function getSharedDirectories(): array;

}

<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Builder;

use Geeks4Change\GeekyDeploy\FileContent\Php\PhpAssignment;
use Geeks4Change\GeekyDeploy\FileContent\Yaml\YamlContent;
use Geeks4Change\GeekyDeploy\FileContent\Yaml\YamlContentInterface;
use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Geeks4Change\GeekyDeploy\Runner\RunnerInterface;
use Geeks4Change\GeekyDeploy\Server\AlterDrupalSettingsInterface;
use Geeks4Change\GeekyDeploy\Server\ServerInterface;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTargetInterface;
use Geeks4Change\GeekyDeploy\Target\TargetInterface;
use Symfony\Component\Filesystem\Path;

class DrupalBuilder implements BuilderInterface {

  private function __construct(
    protected readonly DrupalSettings $settings,
    protected readonly array $drushYamlData,
    protected readonly ?YamlContentInterface $services = NULL,
    // @todo Webroot is not Drupal specific, make it so.
    protected readonly string $drupalRoot = 'web',
    protected readonly string $multiSiteName = 'default',
  ) {}

  /**
   * @api
   */
  public static function create(
    DrupalSettings $settings,
    array $drushYamlData,
    ?YamlContentInterface $services = NULL,
    string $drupalRoot = 'web',
    string $multiSiteName = 'default',
  ): static {
    return new static($settings, $drushYamlData, $services, $drupalRoot, $multiSiteName);
  }

  final public function getDrupalRoot(): string {
    return $this->drupalRoot;
  }

  final public function getMultiSiteName(): string {
    return $this->multiSiteName;
  }


  /**
   * @api May be overridden.
   */
  public function install(RunnerInterface $runner, bool $reinstall): void {
    $runner->mustRun(['vendor/bin/drush', 'si', '--account-pass=admin', '--existing-config']);
  }

  /**
   * @api May be overridden.
   */
  public function serve(LocalRunnerInterface $localRunner, int $port): void {
    $localRunner->mustRun(['php', '-S', "localhost:$port", "$this->drupalRoot/.ht.router.php"]);
  }

  /**
   * @api May be overridden.
   */
  public function build(LocalRunnerInterface $localRunner, TargetInterface $target): void {
    if ($target->isDev()) {
      $this->composerInstallDev($localRunner);
    }
    else {
      $this->composerInstallNoDev($localRunner);
    }
    $this->generateFiles($localRunner, $target);
  }

  /**
   * @api May be overridden.
   */
  protected function composerInstallDev(LocalRunnerInterface $localRunner): void {
    $localRunner->mustRun(['composer', 'install'], []);
  }

  /**
   * @api May be overridden.
   */
  protected function composerInstallNoDev(LocalRunnerInterface $localRunner): void {
    $localRunner->mustRun(
      ['composer', 'install', '--prefer-dist', '--optimize-autoloader', '--no-ansi', '--no-interaction', '--no-progress'],
      ['COMPOSER_NO_DEV' => 1],
    );
  }

  /**
   * @api May be overridden.
   */
  protected function generateFiles(LocalRunnerInterface $localRunner, TargetInterface $target): void {
    $fs = $localRunner->getFilesystem();

    $server = $target instanceof RemoteTargetInterface ? $target->getServer() : NULL;
    // Takes custom services and server into account.
    $settings = $this->getAlteredSettings($server);

    if ($this->services) {
      $servicesFile = "{$this->drupalRoot}/sites/default/services.custom.yml";
      $servicesWritableHandle = $fs->makeWritableAndGetHandle($servicesFile);
      $fs->dumpFile(
        $servicesFile,
        $this->services->getContent(),
      );
      unset($servicesWritableHandle);
    }

    $settingsFile = "{$this->drupalRoot}/sites/default/settings.php";
    $settingsWritableHandle = $fs->makeWritableAndGetHandle($settingsFile);
    $fs->dumpFile(
      $settingsFile,
      $settings->getContent(),
    );
    unset($settingsWritableHandle);

    $fs->dumpFile(
      'drush/drush.yml',
      YamlContent::create($this->getDrushYamlData($target))->getContent(),
    );
    $fs->dumpFile(
      'drush/sites/self.site.yml',
      $this->getAliasesFile($target)->getContent(),
    );
  }

  public function getAlteredSettings(?ServerInterface $server): DrupalSettings {
    $settings = $this->settings;

    // Announce services.custom.yml settings.php.
    if ($this->services) {
      $settings = $settings->withAssignment(
        PhpAssignment::create("\$settings['container_yamls']['custom']", 'sites/default/services.custom.yml'),
      );
    }

    // Allow server deployment to alter settings.
    if ($server instanceof AlterDrupalSettingsInterface) {
      $server->alterDrupalSettings($settings);
    }
    return $settings;
  }

  public function getSharedDirectories(): array {
    return array_map(
      fn(string $directory) => Path::canonicalize("{$this->drupalRoot}/$directory"),
      $this->settings->getSharedDirectories(),
    );
  }

  /**
   * @api May be overridden.
   */
  protected function getDrushYamlData(TargetInterface $target): array {
    $data = $this->drushYamlData;
    $data['options']['uri'] = $target->getUrl();
    return $data;
  }

  /**
   * @internal
   */
  final protected function getAliasesFile(TargetInterface $target): YamlContent {
    return YamlContent::create($this->getAllAliasesData($target));
  }

  /**
   * @internal
   */
  final protected function getAllAliasesData(TargetInterface $forTarget): array {
    $targets = $forTarget->getTargets();
    $data = [];
    foreach ($targets->getRemoteNames() as $remoteName) {
      $aliasTarget = $targets->getRemote($remoteName);
      // Only remotes go to self.sites.yml.
      // Current local goes to drush.yml.
      $data[$aliasTarget->getName()] = $this->getAliasData($aliasTarget, $forTarget);

      if ($aliasTarget === $forTarget) {
        // When running a command on a remote, the drush.yml on the remote seems not to be picked up by drush.
        // Anyway, it's only there **after** deployment, and we want options to work immediately.
        // So add the command settings from the respective drush.yml too.
        $data[$aliasTarget->getName()]['command'] = $this->drushYamlData['command'] ?? NULL;
      }
    }
    return $data;
  }

  /**
   * @api May be overridden.
   */
  protected function getAliasData(RemoteTargetInterface $aliasTarget, TargetInterface $forTarget): array {
    $server = $aliasTarget->getServer();
    $sshInfo = $server->getSshInfo($aliasTarget);
    // https://www.drush.org/12.x/examples/example.site.yml/
    $data['uri'] = $aliasTarget->getUrl();
    $data['host'] = $sshInfo->host;
    $data['user'] = $sshInfo->user;
    if ($root = $sshInfo->path) {
      $drupalRoot = $this->drupalRoot;
      if ($server instanceof AlterDrupalSettingsInterface) {
        $server->alterDrupalRoot($drupalRoot);
      }
      $data['root'] = "$root/$drupalRoot";
    }
    if ($port = $sshInfo->port) {
      $data['ssh']['options'] = "-p $port";
    }
    return $data;
  }

}

<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Builder;

use Symfony\Component\Yaml\Yaml;

final class DrupalDefault {

  public static function DrushYaml(): array {
    return Yaml::parse(<<<EOD
    command:
      sql:
        dump:
          options:
            structure-tables-key: dump
            skip-tables-key: dump
            # https://support.acquia.com/hc/en-us/articles/1500002909602-Drush-throws-an-Access-denied-you-need-at-least-one-of-the-PROCESS-privilege-s-error-message
            # https://github.com/drush-ops/drush/issues/4410#issuecomment-782949114
            extra-dump: --no-tablespaces --column-statistics=0
        sync:
          options:
            structure-tables-key: sync
            skip-tables-key: sync
    ssh:
      options: '-o PasswordAuthentication=no'
      # This string is valid for Bash shell. Override in case you need something different. See https://github.com/drush-ops/drush/issues/3816.
      pipefail: 'set -o pipefail; '
    sql:
      structure-tables:
        sync: &structure-tables
          - 'cache'
          - 'cache_*'
          - 'history'
          - 'search_*'
          - 'sessions'
          - 'watchdog'
        dump: *structure-tables
      skip-tables:
        dump: []
        # Don't sync migration tables, but dump them.
        sync:
          - 'migration_*'
    EOD
    );
  }

  public static function SqliteDb(): array {
    return [
      'driver' => 'sqlite',
      'database' => 'sites/default/files/.ht.sqlite',
    ];
  }

}

<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner\Test;

use Geeks4Change\GeekyDeploy\RelativeFilesystem\RelativeFilesystem;
use Geeks4Change\GeekyDeploy\Runner\RemoteRunnerInterface;
use Geeks4Change\GeekyDeploy\Runner\Utility\EscapeArgTrait;
use Geeks4Change\GeekyDeploy\Runner\Utility\CreateCommandLineTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;
use Symfony\Component\Process\Process;

abstract class TestRunner implements RemoteRunnerInterface {


  use EscapeArgTrait;
  use CreateCommandLineTrait;

  protected readonly Filesystem $filesystem;
  protected readonly string $directory;
  protected readonly string $logFile;
  protected readonly Process $cachedSuccessfulProcess;

  public function __construct(
    string $directory,
  ) {
    $this->directory = Path::makeAbsolute($directory ?? '.', getcwd());
    $this->logFile = "$this->directory/runner.log";
    $this->filesystem = new RelativeFilesystem($directory, $this->writeLog(...));
    $process = new Process(['true']);
    $process->run();
    $this->cachedSuccessfulProcess = $process;
  }

  public function forWorkingDirectory(string $directory): static {
    return new static(
      Path::makeAbsolute($directory, $this->directory),
    );
  }


  public function run(array|string $command, array $env = []): Process {
    $commandline = $this->createCommandLine($command, $env);
    $this->writeLog("?$ $commandline");
    return $this->cachedSuccessfulProcess;
  }

  public function mustRun(array|string $command, array $env = []): Process {
    $commandline = $this->createCommandLine($command, $env);
    $this->writeLog("!$ $commandline");
    return $this->cachedSuccessfulProcess;
  }

  protected function writeLog(string $log): void {
    (new Filesystem())->appendToFile($this->logFile, "$log\n");
  }

}

<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner\Test;

use Geeks4Change\GeekyDeploy\Runner\Remote\Ssh\SshInfo;
use Geeks4Change\GeekyDeploy\Runner\Remote\Ssh\SshRunnerFactoryInterface;
use Geeks4Change\GeekyDeploy\Runner\RemoteRunnerInterface;
use Symfony\Component\Filesystem\Path;

final class TestSshRunnerFactory implements SshRunnerFactoryInterface {

  protected readonly string $directory;

  public function __construct(
    string $directory,
  ) {
    $this->directory = Path::makeAbsolute($directory ?? '.', getcwd());
  }

  public function createRunner(SshInfo $sshInfo): RemoteRunnerInterface {
    return new TestRemoteRunner($this->directory);
  }

}

<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner\Test;

use Geeks4Change\GeekyDeploy\Runner\RemoteRunnerInterface;

final class TestRemoteRunner extends TestRunner implements RemoteRunnerInterface {}

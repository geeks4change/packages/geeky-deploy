<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner\Test;

use Geeks4Change\GeekyDeploy\RelativeFilesystem\RelativeFilesystem;
use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;

final class TestLocalRunner extends TestRunner implements LocalRunnerInterface {

  /**
   * @return \Geeks4Change\GeekyDeploy\RelativeFilesystem\RelativeFilesystem
   */
  public function getFilesystem(): RelativeFilesystem {
    return $this->filesystem;
  }

}

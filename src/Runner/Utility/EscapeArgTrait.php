<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner\Utility;

trait EscapeArgTrait {

  /** Taken from @see \Symfony\Component\Process\Process::escapeArgument */
  private function escapeArgument(?string $argument): string {
    if ('' === $argument || null === $argument) {
      return '""';
    }
    if ('\\' !== \DIRECTORY_SEPARATOR) {
      return "'".str_replace("'", "'\\''", $argument)."'";
    }
    if (str_contains($argument, "\0")) {
      $argument = str_replace("\0", '?', $argument);
    }
    if (!preg_match('/[\/()%!^"<>&|\s]/', $argument)) {
      return $argument;
    }
    $argument = preg_replace('/(\\\\+)$/', '$1$1', $argument);

    return '"'.str_replace(['"', '^', '%', '!', "\n"], ['""', '"^^"', '"^%"', '"^!"', '!LF!'], $argument).'"';
  }

}

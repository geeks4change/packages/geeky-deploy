<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner\Utility;

use Symfony\Component\Process\Process;

trait CreateCommandLineTrait {

  private function createCommandLine(string|array $command, array $env  = []): string {
    $commandArray = (array) $command;
    if ($env) {
      $envPrefix = [];
      foreach ($env as $variable => $value) {
        // Process helper will do the quoting later.
        $envPrefix[] = "$variable=$value";
      }
      $commandArray = array_merge(['env'], $envPrefix, $commandArray);
    }
    return (new Process($commandArray))->getCommandLine();
  }

}

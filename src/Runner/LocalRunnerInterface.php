<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner;

use Geeks4Change\GeekyDeploy\RelativeFilesystem\RelativeFilesystem;

interface LocalRunnerInterface extends RunnerInterface {

  public function getFilesystem(): RelativeFilesystem;

}

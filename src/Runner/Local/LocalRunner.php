<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner\Local;

use Geeks4Change\GeekyDeploy\RelativeFilesystem\RelativeFilesystem;
use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Path;
use Symfony\Component\Process\Process;

final class LocalRunner implements LocalRunnerInterface {

  private readonly RelativeFilesystem $filesystem;
  private readonly ?string $directory;

  public function __construct(
    ?string $directory,
    private readonly LoggerInterface $logger,
    private readonly ?int $timeout = NULL,
  ) {
    $this->directory = Path::makeAbsolute($directory ?? '.', getcwd());
    $this->filesystem = new RelativeFilesystem($this->directory, $this->logger);
  }

  public function getLogger(): LoggerInterface {
    return $this->logger;
  }

  public function forWorkingDirectory(string $directory): static {
    return new self(
      Path::makeAbsolute($directory, $this->directory),
      $this->logger,
      $this->timeout,
    );
  }

  public function run(array|string $command, array $env = []): Process {
    $process = new Process(
      command: (array) $command,
      cwd: $this->directory,
      env: $env,
      input: NULL,
      timeout: $this->timeout,
    );
    $this->logger->info(sprintf('?$ %s', $process->getCommandLine()));
    $process->run();
    return $process;
  }

  public function mustRun(array|string $command, array $env = []): Process {
    $process = new Process(
      command: (array) $command,
      cwd: $this->directory,
      env: $env,
      input: NULL,
      timeout: $this->timeout,
    );
    $this->logger->info(sprintf('!$ %s', $process->getCommandLine()));
    $process->mustRun();
    return $process;
  }

  public function getFilesystem(): RelativeFilesystem {
    return $this->filesystem;
  }

}

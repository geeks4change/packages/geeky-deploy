<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner;

use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

interface RunnerInterface {

  public function run(string|array $command, array $env = []): Process;

  public function mustRun(string|array $command, array $env = []): Process;

  public function forWorkingDirectory(string $directory): static;

  public function getLogger(): LoggerInterface;

}

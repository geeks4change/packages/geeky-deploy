<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner;

interface RemoteRunnerInterface extends RunnerInterface {}

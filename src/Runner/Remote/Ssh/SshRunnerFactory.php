<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner\Remote\Ssh;

use Geeks4Change\GeekyDeploy\Runner\RemoteRunnerInterface;
use Psr\Log\LoggerInterface;

final class SshRunnerFactory implements SshRunnerFactoryInterface {

  private function __construct(
    private readonly LoggerInterface $logger,
  ) {}

  public static function create(LoggerInterface $logger): self {
    return new self($logger);
  }

  public function createRunner(SshInfo $sshInfo): RemoteRunnerInterface {
    return new SshRunner($sshInfo, $this->logger);
  }

}

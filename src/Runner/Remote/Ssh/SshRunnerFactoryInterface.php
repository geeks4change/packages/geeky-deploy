<?php

namespace Geeks4Change\GeekyDeploy\Runner\Remote\Ssh;

use Geeks4Change\GeekyDeploy\Runner\RemoteRunnerInterface;

interface SshRunnerFactoryInterface {

  public function createRunner(SshInfo $sshInfo): RemoteRunnerInterface;

}
<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner\Remote\Ssh;

use Geeks4Change\GeekyDeploy\Runner\RemoteRunnerInterface;
use Geeks4Change\GeekyDeploy\Runner\Utility\CreateCommandLineTrait;
use Psr\Log\LoggerInterface;
use Spatie\Ssh\Ssh;
use Symfony\Component\Filesystem\Path;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

final class SshRunner implements RemoteRunnerInterface {

  use CreateCommandLineTrait;

  public function __construct(
    private readonly SshInfo $sshInfo,
    private readonly LoggerInterface $logger,
    private readonly ?int $timeout = NULL,
  ) {}

  public function getLogger(): LoggerInterface {
    return $this->logger;
  }

  public function forWorkingDirectory(string $directory): static {
    return new self(
      sshInfo: new SshInfo(
        host: $this->sshInfo->host,
        user: $this->sshInfo->user,
        port: $this->sshInfo->port,
        path: Path::makeAbsolute($directory, $this->sshInfo->path),
      ),
      logger: $this->logger,
      timeout: $this->timeout,
    );
  }

  public function run(array|string $command, array $env = []): Process {
    $process = $this->getSsh()->execute($this->createSshLines($command, $env));
    $this->logger->info(sprintf('?$ %s', $process->getCommandLine()));
    return $process;
  }

  public function mustRun(array|string $command, array $env = []): Process {
    $process = $this->getSsh()->execute($this->createSshLines($command, $env));
    $this->logger->info(sprintf('!$ %s', $process->getCommandLine()));
    if ($process->getExitCode() !== 0) {
      /** @see \Symfony\Component\Process\Process::mustRun */
      throw new ProcessFailedException($process);
    }
    return $process;
  }

  public function createSshLines(array|string $command, array $env): array {
    $command = (array) $command;
    if ($this->sshInfo->path) {
      // Quote and escape path, but not '&&'.
      $maybeChangeDirectory = $this->createCommandLine(['cd', $this->sshInfo->path]) . ' && ';
    }
    else {
      $maybeChangeDirectory = '';
    }
    $commandLine = $this->createCommandLine($command, $env);
    return ["$maybeChangeDirectory$commandLine"];
  }

  private function getSsh(): Ssh {
    $ssh = Ssh::create($this->sshInfo->user, $this->sshInfo->host, $this->sshInfo->port);
    if (isset($this->timeout)) {
      $ssh->setTimeout($this->timeout);
    }
    return $ssh;
  }

}

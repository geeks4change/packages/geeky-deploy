<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Runner\Remote\Ssh;

final class SshInfo {

  public readonly ?string $path;

  public function __construct(
    public readonly string $host,
    public readonly string $user,
    public readonly int $port = 22,
    ?string $path = NULL,
  ) {
    $this->path = isset($path) ? rtrim($path, '/') : NULL;
  }

}

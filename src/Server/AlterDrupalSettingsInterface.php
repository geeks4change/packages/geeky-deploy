<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Server;

use Geeks4Change\GeekyDeploy\Builder\DrupalSettings;

interface AlterDrupalSettingsInterface {

  public function alterDrupalSettings(DrupalSettings &$settings): void;

  public function alterDrupalRoot(string &$drupalRoot): void;

}

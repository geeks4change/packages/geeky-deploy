<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Server;

use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Geeks4Change\GeekyDeploy\Runner\Remote\Ssh\SshInfo;
use Geeks4Change\GeekyDeploy\Runner\RemoteRunnerInterface;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTarget;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTargetInterface;

interface ServerInterface {

  public function getSshInfo(RemoteTargetInterface $target): SshInfo;

  public function beforeBuild(RemoteTarget $target, LocalRunnerInterface $sandboxRunner): void;

  public function releasePrepare(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void;

  public function releaseSwitch(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void;

  public function releaseUnlock(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void;

}

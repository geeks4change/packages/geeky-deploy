<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Server\GitDeployment;

use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Symfony\Component\Finder\Finder;

trait GitDeploymentServerTrait {

  private function deleteDotGitDirs(LocalRunnerInterface $localRunner): void {
    $finder = new Finder();
    $finder->directories()
      ->name('.git')
      ->in($localRunner->getFilesystem()->getCurrentDir());
    foreach ($finder as $file) {
      unlink($file->getRealPath());
    }
  }

  private function prepareGitDeployment(LocalRunnerInterface $localRunner): void {
    $localRunner->mustRun(['git', 'init']);
    $userNameMissing = $localRunner->run(['git', 'config', 'user.name'])->getExitCode();
    if ($userNameMissing) {
      $localRunner->mustRun(['git', 'config', 'user.name', 'GeekyDeploy']);
    }
    $userEmailMissing = $localRunner->run(['git', 'config', 'user.email'])->getExitCode();
    if ($userEmailMissing) {
      $localRunner->mustRun(['git', 'config', 'user.email', 'geeky-deploy@example.com']);
    }
    $localRunner->mustRun(['git', 'config', 'core.safecrlf', 'false']);
    $localRunner->mustRun(['git', 'config', 'init.defaultBranch', 'master']);
    $localRunner->mustRun(['git', 'remote', 'add', 'origin', $this->getRemoteGitUrl()]);
    $localRunner->mustRun(['git', 'fetch', '--depth', '1', '--update-head-ok', 'origin', "master:{$this->getRemoteGitBranch()}"]);
    $localRunner->mustRun(['git', 'branch', '-u', "origin/{$this->getRemoteGitBranch()}"]);
    $localRunner->mustRun(['git', 'add', '--all', '--force']);
    $localRunner->mustRun(['git', 'commit', '-m', 'GeekyDeploy', '--allow-empty', '--quiet']);
  }

  abstract private function getRemoteGitUrl(): string;
  abstract private function getRemoteGitBranch(): string;

}

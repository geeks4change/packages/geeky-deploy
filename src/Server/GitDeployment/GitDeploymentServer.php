<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Server\GitDeployment;

use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Geeks4Change\GeekyDeploy\Runner\Remote\Ssh\SshInfo;
use Geeks4Change\GeekyDeploy\Runner\RemoteRunnerInterface;
use Geeks4Change\GeekyDeploy\Server\ServerInterface;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTarget;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTargetInterface;

final class GitDeploymentServer implements ServerInterface {

  use GitDeploymentServerTrait;

  public function __construct(
    protected readonly SshInfo $sshInfo,
    protected readonly string $remoteGitUrl,
    protected readonly string $remoteGitBranch,
  ) {}

  public function getSshInfo(RemoteTargetInterface $target): SshInfo {
    return $this->sshInfo;
  }

  public function beforeBuild(RemoteTarget $target, LocalRunnerInterface $sandboxRunner): void {}

  public function releasePrepare(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {
    $this->deleteDotGitDirs($sandboxRunner);
    $this->prepareGitDeployment($sandboxRunner);
  }

  public function releaseSwitch(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {
    $sandboxRunner->mustRun(['git', 'push']);
  }

  public function releaseUnlock(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {
    // Nothing to do.
  }

  private function getRemoteGitUrl(): string {
    return $this->remoteGitUrl;
  }

  private function getRemoteGitBranch(): string {
    return $this->remoteGitBranch;
  }

}

<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Server\Fsb;

use Geeks4Change\GeekyDeploy\Builder\DrupalBuilder;
use Geeks4Change\GeekyDeploy\Builder\DrupalSettings;
use Geeks4Change\GeekyDeploy\RelativeFilesystem\RelativeFilesystem;
use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Geeks4Change\GeekyDeploy\Runner\Remote\Ssh\SshInfo;
use Geeks4Change\GeekyDeploy\Runner\RemoteRunnerInterface;
use Geeks4Change\GeekyDeploy\Server\AlterDrupalSettingsInterface;
use Geeks4Change\GeekyDeploy\Server\GitDeployment\GitDeploymentServerTrait;
use Geeks4Change\GeekyDeploy\Server\ServerInterface;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTarget;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTargetInterface;
use Geeks4Change\GeekyDeploy\Utility\SshKnownHostFile;

final class FsbServer implements ServerInterface, AlterDrupalSettingsInterface {

  use GitDeploymentServerTrait;

  const DrupalRoot = 'docroot';

  private function __construct(
    private readonly string $siteHandle,
    private readonly string $sshServerPrefix,
    private readonly string $remoteGitBranch = 'master',
  ) {}

  public static function create(
    string $siteHandle,
    string $sshServerPrefix,
    string $remoteGitBranch = 'master',
  ): self {
    return new self($siteHandle, $sshServerPrefix, $remoteGitBranch);
  }

  private function getSiteDir(): string {
    return "/srv/www/freistilbox/home/{$this->siteHandle}/current";
  }

  private function getSshHost(): string {
    return "{$this->sshServerPrefix}.freistilbox.net";
  }

  private function getRemoteGitUrl(): string {
    return "ssh://{$this->siteHandle}@repo.freistilbox.net/~/site";
  }

  private function getRemoteGitBranch(): string {
    return $this->remoteGitBranch;
  }

  public function getSshInfo(RemoteTargetInterface $target): SshInfo {
    return new SshInfo(
      host: $this->getSshHost(),
      user: $this->siteHandle,
      // @todo Add option to get webRoot / drupalRoot (which may be different).
      path: $this->getSiteDir(),
    );
  }

  public function beforeBuild(RemoteTarget $target, LocalRunnerInterface $sandboxRunner): void {}

  public function releasePrepare(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {
    // Remove shared directories before moving docroot.
    $this->removeSharedDirectories($target, $sandboxRunner->getFilesystem());
    $this->symlinkDocroot($target, $sandboxRunner);
    // Adjust composer autoloader. Doing the build in new docroot is not an
    // option, as the original drupalRoot is assumed in too many places, like
    // module install dirs.
    // No, this does not work! Is obsoleted by moveDocroot's back symlink.
    // $sandboxRunner->run(['composer', 'dump-autoload', '--optimize', '--apcu', '--no-ansi'],);
    $this->moveConfigSync($sandboxRunner);

    $this->ensureBoxfile($sandboxRunner);
    $this->ensureKnownHosts($sandboxRunner);
    $this->deleteDotGitDirs($sandboxRunner);
    $this->prepareGitDeployment($sandboxRunner);
  }

  public function releaseSwitch(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {
    $sandboxRunner->mustRun(['git', 'push']);
    $this->waitUntilReleased($target, $sandboxRunner, $remoteRunner);
  }

  public function releaseUnlock(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {
    // Nothing to do.
  }

  public function alterDrupalRoot(string &$drupalRoot): void {}

  public function alterDrupalSettings(DrupalSettings &$settings): void {
    $settings = $settings
      ->withFileTempPath('../tmp')
      // @todo Pick up DrupalSettings private path.
      ->withFilePrivatePath('../private/sites/default')
      ->withPhpLine(<<<EOD
        require DRUPAL_ROOT . '/../config/drupal/settings-d8-site.php';
        require glob(DRUPAL_ROOT . '/../config/drupal/settings-d8-db*.php')[0]
          ?? throw new \Exception('DB missing! Must be created in dashboard (and needs some minutes to arrive).');
        if (!file_exists(DRUPAL_ROOT . '/../private/sites/default')) {
          mkdir(DRUPAL_ROOT . '/../private/sites/default');
        }
          
        EOD
    );
    /** @see self::moveConfigSync */
    if ($settings->configSyncDirectory === '../config/sync') {
      $settings = $settings->withConfigSyncDirectory('../config-sync');
    }
  }

  private function symlinkDocroot(RemoteTargetInterface $target, LocalRunnerInterface $localRunner): void {
    $builder = $target->getBuilder();
    /** @noinspection PhpConditionAlreadyCheckedInspection */
    if (
      $builder instanceof DrupalBuilder
      && $builder->getDrupalRoot() !== self::DrupalRoot
    ) {
      $localRunner->getLogger()->info(sprintf("FSB needs docroot so symlinking '%s' => '%s'.", self::DrupalRoot, $builder->getDrupalRoot()));
      $fs = $localRunner->getFilesystem();
      if (
        $fs->isDir($builder->getDrupalRoot())
        && !$fs->isFile(self::DrupalRoot)
      ) {
        // First arg is target of (relative) symlink.
        $fs->symlink($builder->getDrupalRoot(), self::DrupalRoot);
      }
    }
  }

  public function removeSharedDirectories(RemoteTargetInterface $target, RelativeFilesystem $fs): void {
    foreach ($target->getBuilder()->getSharedDirectories() as $sharedDir) {
      if ($fs->exists($sharedDir)) {
        $fs->remove($sharedDir);
      }
    }
  }

  private function ensureBoxfile(LocalRunnerInterface $sandboxRunner): void {
    $fs = $sandboxRunner->getFilesystem();
    if (!$fs->exists('Boxfile')) {
      $fs->dumpFile('Boxfile', $this->getBoxfileContent());
    }
  }

  private function getBoxfileContent(): string {
    return <<<'EOS'
version: 2.0
shared_folders:
  - docroot/sites/default/files

EOS;
  }

  private function moveConfigSync(LocalRunnerInterface $sandboxRunner): void {
    $fs = $sandboxRunner->getFilesystem();
    if (!$fs->exists('config-sync') && $fs->isDir('config/sync')) {
      $fs->rename('config/sync', 'config-sync');
    }
  }

  private function waitUntilReleased(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {
    $homeDir = trim($remoteRunner->mustRun(['printenv', 'HOME'])->getOutput());
    $remoteRunner->getLogger()->info("Found home dir: '$homeDir'");

    if (!$homeDir) {
      throw new \UnexpectedValueException('Can not find remote home directory.');
    }

    $gitHashOutput = trim($sandboxRunner->mustRun(['git', 'rev-parse', 'HEAD'])->getOutput());
    $releaseHash = basename($gitHashOutput);
    $remoteRunner->getLogger()->info("Waiting for release hash: '$releaseHash'");

    foreach (range(1, 16) as $i) {
      // On first deploy, there is no current, and this fails.
      $releaseSymlink = $remoteRunner->run(['readlink', "$homeDir/.deploy/current"])
        ->getOutput();
      $currentHash = basename(trim($releaseSymlink));
      $remoteRunner->getLogger()->info("Found hash: '$currentHash'");
      if (trim($currentHash) === $releaseHash) {
        $remoteRunner->getLogger()->info("Success: Found release hash.");
        return;
      }
      sleep(2);
    }
    // $this->logger->error("Deployment seems to have failed. Deploy log:");
    $deployLog = $remoteRunner->run(['tail', '-n', '10', "$homeDir/.deploy/deploy.log"])
      ->getOutput();
    throw new \RuntimeException("Deploy error: $deployLog");
  }

  public function ensureKnownHosts(LocalRunnerInterface $sandboxRunner): void {
    $knownHostsToAdd = $this->getExpectedKnownHosts();
    foreach ($knownHostsToAdd as $host => $expectedHostKey) {
      $sandboxRunner->getLogger()->debug("For host '$host' expecting key: '$expectedHostKey'.");
      $lookup = $sandboxRunner->run(['ssh-keygen', '-F', $host]);
      if ($lookup->getExitCode() === 0) {
        $output = $lookup->getOutput();
        // first line is: Host repo.freistilbox.net found: line 23
        [$foundLine, $hostLine] = explode("\n", $output);
        [$hostOrHash, $hostKey] = explode(' ', trim($hostLine), 2);
        $sandboxRunner->getLogger()->debug("For host '$host' found key:     '$hostKey'.");
        if ($hostKey === $expectedHostKey) {
          $sandboxRunner->getLogger()->info("For host '$host' key matches, nothing to do.");
          unset($knownHostsToAdd[$host]);
        }
        else {
          $sandboxRunner->getLogger()->info("For host '$host' key does not match, replacing.");
          $sandboxRunner->run(['ssh-keygen', '-R', $host]);
        }
      }
      else {
        $sandboxRunner->getLogger()->info("For host '$host' found NO key, adding.");
      }
    }
    if ($knownHostsToAdd) {
      SshKnownHostFile::add($knownHostsToAdd);
    }
  }

  private function getExpectedKnownHosts(): array {
    return [
      'repo.freistilbox.net' => 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC350pWJ3Lx9OwCqNrYxSXW8J6zpm0Osm8e+BEFkAH25/lM2s4ssygjmjE8HzOf959sRe402rBjiSr1mWTsXk43WaftyVxugyA7B12L8bk6V+O63Ugc90+YuezSyjggXDujF24kQr9MsE+cpLhbV6lDAGAspXbRZP0RtFgAEiKeZs45uBNTqZecMfg/vMM/VaQsqZyC17bN+eqRs6dSDLb8LhQvX0NX0ep67FEoMka16OmdIDfZgHAn2p11zCqgLaBqUK3OKbSmPZSQXTjT88WJlPSp3EDt2Y+IKAbzfYCnWibiNfR2XOejhebP2dgRZdEJp0DQr12aexvWwWklQg5EIJCuh+tbCgPJkSr3PZwnmQfZ8XFyIefU3fkZU9ridO6m666UTqPohJKMUjRw+6e7as8O+5NvIUcZ5lhhuRwe9LDUJrs5ECoLWemHOqjhRPpP66ABU88ZhDz5WIcNtjUOmbYzj7RlQotIYdMg3vjgytKsC4Xg55+8q2M1OcXkrVk=',
      'c145s.freistilbox.net' => 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCtHSoxOi09T0ZywNxVHiQaKp3mvFfIKadg7Z3GFC/VZEkNYyf1bYB5PGHGQhuq4xfx4/D8wwyP/SLswqq+TkFM5j8frvdjTQGsSj76744EdMHgcUgKs4wjdc6ivlbxf9SfylSlF+2g/o9Lv2887NNtYs9GkW6YXJxmI7IBJwiDz/EgskWGVopqFeoNNQ71suEdxJzs8he1OmNO9kA3kVASlU570uK81ThLJRDDQb2UdKV/6+/wX9CZdoD6sheeZ811ntgDCJer6qkfI30l572DJfm1FFZCwXQ2rEPhGSry8swy2divSSxWXeu+eTDGRYmYcgDk9bv4uZnR+iP8X2jBjqpUVQfDOLP9QM8siYLaQxBgUGOoFe3DW0S4CIZaeeK/zJ3yi8fJ0CrCL+qVzD+bvYH4Virjzm+SkMj+B1YomUfDeNmrKhv1eFN23tAZlrLu2k7y9d9hWeoO9zBkMRnAdFER/zJiCL+P03xGzBOdnXVgNFpkNSBMewaGcIzLU1M=',
    ];
  }

}

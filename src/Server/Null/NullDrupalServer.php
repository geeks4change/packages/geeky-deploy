<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Server\Null;

use Geeks4Change\GeekyDeploy\Builder\DrupalSettings;
use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Geeks4Change\GeekyDeploy\Runner\Remote\Ssh\SshInfo;
use Geeks4Change\GeekyDeploy\Runner\RemoteRunnerInterface;
use Geeks4Change\GeekyDeploy\Server\AlterDrupalSettingsInterface;
use Geeks4Change\GeekyDeploy\Server\ServerInterface;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTarget;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTargetInterface;

final class NullDrupalServer implements ServerInterface, AlterDrupalSettingsInterface {

  public function __construct(
    private readonly SshInfo $sshInfo,
  ) {}

  public function getSshInfo(RemoteTargetInterface $target): SshInfo {
    return $this->sshInfo;
  }

  public function alterDrupalSettings(DrupalSettings &$settings): void {}

  public function alterDrupalRoot(string &$drupalRoot): void {}

  public function beforeBuild(RemoteTarget $target, LocalRunnerInterface $sandboxRunner): void {}

  public function releasePrepare(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {}

  public function releaseSwitch(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {}

  public function releaseUnlock(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {}

}

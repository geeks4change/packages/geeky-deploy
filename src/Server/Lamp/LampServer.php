<?php

declare(strict_types=1);
namespace Geeks4Change\GeekyDeploy\Server\Lamp;

use Geeks4Change\GeekyDeploy\Runner\LocalRunnerInterface;
use Geeks4Change\GeekyDeploy\Runner\Remote\Ssh\SshInfo;
use Geeks4Change\GeekyDeploy\Runner\RemoteRunnerInterface;
use Geeks4Change\GeekyDeploy\Server\ServerInterface;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTarget;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTargetInterface;
use Geeks4Change\GeekyDeploy\Utility\OnShutdown\OnShutdown;
use Geeks4Change\GeekyDeploy\Utility\PhpUnitTool;

/**
 * Capistrano style deployment for arbitrary LAMP server having SSH.
 */
final class LampServer implements ServerInterface {

  protected readonly string $releaseName;
  protected string $previousReleaseName;

  private OnShutdown $dropLock;

  private OnShutdown $dropReleaseDir;

  public function __construct(
    protected readonly SshInfo $sshInfo,
  ) {
    if (PhpUnitTool::isRunningTest()) {
      $this->releaseName = 'test_release_name';
    }
    else {
      $this->releaseName = (new \DateTimeImmutable())
        ->format('Y-m-d-H-i-s');
    }
  }

  public function getSshInfo(RemoteTargetInterface $target): SshInfo {
    return $this->sshInfo;
  }

  public function beforeBuild(RemoteTarget $target, LocalRunnerInterface $sandboxRunner): void {}

  public function releasePrepare(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {
    // Ensure required directories.
    $remoteRunner->mustRun(['mkdir', '-p', 'releases']);
    $remoteRunner->mustRun(['mkdir', '-p', 'shared']);
    $remoteRunner->mustRun(['mkdir', "releases/$this->releaseName"]);
    $this->dropReleaseDir = OnShutdown::create(
      fn() => $remoteRunner->mustRun(['rm', '-rf', "releases/$this->releaseName"])
    );

    // Get previous release name to keep it later.
    $this->previousReleaseName = trim($remoteRunner->mustRun(['readlink', 'current'])
      ->getOutput());

    // Create the 'releasing' symlink as a lock.
    // Do it atomically: ln is not atomic, but mv is.
    // https://temochka.com/blog/posts/2017/02/17/atomic-symlinks.html
    // Ensure the lock is removed on shutdown.
    // If releases/releasing is left over, it will be silently ignored.
    $remoteRunner->mustRun(['ln', '--symbolic', '--no-dereference', '--force', '--relative', "releases/$this->releaseName", 'releases/releasing']);
    $remoteRunner->run(['mv', 'releases/releasing', 'releasing'])
      ->getExitCode() === 0 || throw new \RuntimeException('Can not get releasing lock.');
    $this->dropLock = OnShutdown::create(
      fn() => $remoteRunner->mustRun(['rm', '-rf', "releasing"])
    );

    $sandboxRunner->mustRun("rsync . XXX");

    // Create and link shared dirs.
    foreach ($target->getBuilder()->getSharedDirectories() as $sharedDir) {
      $remoteRunner->mustRun(['mkdir', '-p', "shared/$sharedDir"]);
      $remoteRunner->mustRun(['ln', '--symbolic', '--no-dereference', '--force', '--relative', "shared/$sharedDir", "releasing/$sharedDir"]);
    }

    // Finally write-protect the release.
    $remoteRunner->mustRun(['chmod', '-R', 'a-w', "releases/$this->releaseName"]);
  }

  public function releaseSwitch(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {
    // Switch the 'current' symlink.
    // Do it atomically: ln is not atomic, but mv is.
    $remoteRunner->mustRun(['ln', '--symbolic', '--no-dereference', '--force', '--relative', "releases/{$this->releaseName}", 'releases/current']);
    $remoteRunner->mustRun(["mv -f releases/current ."]);

    // Drop older releases
    $remoteRunner->mustRun(['find', 'releases/*', '-type', 'd', '-not', '-name', $this->releaseName, '-and', '-not', '-name', $this->previousReleaseName, '-delete']);

  }

  public function releaseUnlock(RemoteTargetInterface $target, LocalRunnerInterface $sandboxRunner, RemoteRunnerInterface $remoteRunner): void {
    $this->dropReleaseDir->abandon();
    $this->dropLock->run();
  }

}

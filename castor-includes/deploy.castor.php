<?php

use Castor\Attribute\AsArgument;
use Castor\Attribute\AsOption;
use Castor\Attribute\AsTask;
use Geeks4Change\GeekyDeploy\Runner\Local\LocalRunner;
use Geeks4Change\GeekyDeploy\Runner\Remote\Ssh\SshRunnerFactory;
use Geeks4Change\GeekyDeploy\Target\Targets;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Filesystem\Path;
use function Castor\context;
use function Castor\logger;

function _geekyDeployProjectRoot(): string {
  // Castor provides this in $context->workingDirectory, but not in includes.
  /** @see \Castor\PathHelper::getRoot */
  static $root;

  if (null === $root) {
    $path = getcwd() ?: '/';

    while (!file_exists($path . '/castor.php')) {
      if ('/' === $path) {
        throw new \RuntimeException('Could not find root "castor.php" file.');
      }

      $path = Path::getDirectory($path);
    }

    $root = $path;
  }

  return $root;
}

#[AsTask]
function build(?string $localTarget = NULL) {
  function_exists('getDeployTargets') || throw new \UnexpectedValueException('Missing targets declaration: function getDeployTargets(): \Geeks4Change\GeekyDeploy\Target\Targets {}');
  $targets = getDeployTargets();
  $targets instanceof Targets || throw new \UnexpectedValueException("Function getDeployTargets must return Targets, got: " . get_debug_type($targets));
  $targets->freeze();

  $localRunner = new LocalRunner(_geekyDeployProjectRoot(), logger());
  $targets->getLocal($localTarget)->build($localRunner);
}

#[AsTask]
function deploy(#[AsOption(mode: InputOption::VALUE_NONE)] $suppressRelease, #[AsOption(mode: InputOption::VALUE_NONE)] $keepSandbox,#[AsArgument] ?string $remoteTarget = NULL) {
  function_exists('getDeployTargets') || throw new \UnexpectedValueException('Missing targets declaration: function getDeployTargets(): \Geeks4Change\GeekyDeploy\Target\Targets {}');
  $targets = getDeployTargets();
  $targets instanceof Targets || throw new \UnexpectedValueException("Function getDeployTargets must return Targets, got: " . get_debug_type($targets));
  $targets->freeze();

  $localRunner = new LocalRunner(_geekyDeployProjectRoot(), logger());
  $remoteRunnerFactory = SshRunnerFactory::create(logger());
  $targets->getRemote($remoteTarget)->deploy($localRunner, $remoteRunnerFactory, $suppressRelease, $keepSandbox);
}

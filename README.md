# Geeky Deploy 6 - the 'final' one.

To enable and use:
- Ensure castor is installed globally: xxx
- In your project:
  - `castor composer require geeks4change/geeky-deploy:^6` to add to separate castor composer install.
  - Copy and adapt files from the example directory:
    - castor.php includes
    - deploy.targets.php

For debugging, use verbosity 2 or 3 e.g. `?castor -vvv deploy TARGET`.

Have fun!

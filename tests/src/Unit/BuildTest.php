<?php

declare(strict_types=1);
namespace Geeks4Change\Tests\GeekyDeploy\Unit;

use Geeks4Change\GeekyDeploy\RelativeFilesystem\RelativeFilesystem;
use Geeks4Change\GeekyDeploy\Runner\Test\TestLocalRunner;
use Geeks4Change\GeekyDeploy\Runner\Test\TestSshRunnerFactory;
use Geeks4Change\GeekyDeploy\Target\Targets;
use Geeks4Change\GeekyDeploy\Utility\MkTemp;
use Geeks4Change\GeekyDeploy\Utility\OnShutdown\OnShutdown;
use Geeks4Change\Tests\GeekyDeploy\Traits\DirectoryAssertable\DirectoryAssertableTrait;
use PHPUnit\Framework\TestCase;

final class BuildTest extends TestCase {

  use DirectoryAssertableTrait;

  /**
   * @dataProvider provideBuild
   */
  public function testBuild(Targets $targets, string $expectedDir) {
    $tmpDir = MkTemp::dir();
    OnShutdown::create(
      fn() => (new RelativeFilesystem())->remove($tmpDir)
    );

    foreach ($targets->getLocalNames() as $localName) {
      $localDir = "$tmpDir/$localName/";
      mkdir($localDir);
      $runner = new TestLocalRunner($localDir);
      $targets->getLocal($localName)->build($runner);
    }
    foreach ($targets->getRemoteNames() as $remoteName) {
      $remoteDir = "$tmpDir/$remoteName";
      mkdir($remoteDir);
      $remoteLocalDir = "$remoteDir/local";
      mkdir($remoteLocalDir);
      $localRunner = new TestLocalRunner($remoteLocalDir);
      $remoteRemoteDir = "$remoteDir/remote";
      mkdir($remoteRemoteDir);
      $sshRunnerFactory = new TestSshRunnerFactory($remoteRemoteDir);
      $targets->getRemote($remoteName)->deploy($localRunner, $sshRunnerFactory);
    }

    $this->assertDirectoryEquals($expectedDir, $tmpDir);
  }

  public static function provideBuild() {
    foreach (glob(__DIR__ . '/Fixtures/*') as $dir) {
      $targets = include("$dir/targets.php");
      yield [$targets, "$dir/expected"];
    }
  }

}

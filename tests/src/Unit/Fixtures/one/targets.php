<?php

use Geeks4Change\GeekyDeploy\Builder\DrupalBuilder;
use Geeks4Change\GeekyDeploy\Builder\DrupalDefault;
use Geeks4Change\GeekyDeploy\Builder\DrupalSettings;
use Geeks4Change\GeekyDeploy\Runner\Remote\Ssh\SshInfo;
use Geeks4Change\GeekyDeploy\Server\Null\NullDrupalServer;
use Geeks4Change\GeekyDeploy\Target\Local\LocalTarget;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTarget;
use Geeks4Change\GeekyDeploy\Target\Targets;

$targets = Targets::create();

$drushYamlData = DrupalDefault::DrushYaml();

$settings = DrupalSettings::create(
  hashSalt: 'xxx some secret string xxx'
);

$builder = DrupalBuilder::create(
  settings: $settings
    ->withFilePrivatePath('sites/default/files/private')
    ->withDatabase(DrupalDefault::SqliteDb()),
  drushYamlData: $drushYamlData + ['drush_extra_setting' => 'foo'],
);
$targets->addLocal(LocalTarget::create(
  name: 'local',
  port: 9999,
  builder: $builder,
));

$targets->addRemote(RemoteTarget::create(
  name: 'live',
  url: 'https://www.example.com/',
  server: new NullDrupalServer(new SshInfo('', '')),
  builder: $builder,
));

return $targets->freeze();

<?php

declare(strict_types=1);

namespace Geeks4Change\Tests\GeekyDeploy\Traits\DirectoryAssertable;

use Geeks4Change\Tests\GeekyDeploy\Traits\DirectoryAssertable\Tree\DirItem;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;

trait DirectoryAssertableTrait {
  protected function assertDirectoryEquals(string $expectedDirectory, string $outputDirectory): void {
    $expectedDirectory = Path::makeAbsolute($expectedDirectory, getcwd());
    $outputDirectory = Path::makeAbsolute($outputDirectory, getcwd());

    if ($this->mustUpdate()) {
      (new Filesystem())->mirror($outputDirectory, $expectedDirectory, options: ['delete' => TRUE]);
    }

    $expectedTree = DirItem::create($expectedDirectory, $expectedDirectory);
    $outputTree = DirItem::create($outputDirectory, $outputDirectory);

    $this->assertEquals($expectedTree, $outputTree);
  }

  public function mustUpdate(): bool {
    return boolval(getenv('UPDATE_TEST_OUTPUT'));
  }

}

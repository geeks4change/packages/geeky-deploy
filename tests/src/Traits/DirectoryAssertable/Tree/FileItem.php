<?php

declare(strict_types=1);
namespace Geeks4Change\Tests\GeekyDeploy\Traits\DirectoryAssertable\Tree;

use Symfony\Component\Filesystem\Path;

final class FileItem implements FileTreeInterface {

  public function __construct(
    public readonly string $relPath,
    public readonly string $content,
  ) {}

  public static function create(string $path, $basePath): self {
    is_file($path) || throw new \UnexpectedValueException("ust be file: $path");
    return new self(Path::makeRelative($path, $basePath), file_get_contents($path));
  }

}

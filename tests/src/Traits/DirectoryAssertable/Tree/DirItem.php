<?php

declare(strict_types=1);
namespace Geeks4Change\Tests\GeekyDeploy\Traits\DirectoryAssertable\Tree;

use Symfony\Component\Filesystem\Path;

final class DirItem implements FileTreeInterface {

  /**
   * @param array<\Geeks4Change\Tests\GeekyDeploy\Traits\DirectoryAssertable\Tree\FileTreeInterface> $items
   */
  public function __construct(
    public readonly string $relPath,
    public readonly array $items,
  ) {}

  public static function create(string $path, string $basePath): self {
    is_dir($path) || throw new \UnexpectedValueException("ust be directory: $path");
    $items = [];
    foreach (new \DirectoryIterator($path) as $candidate) {
      if ($candidate->isDot()) {
        continue;
      }
      if ($candidate->isDir()) {
        $item = self::create($candidate->getPathname(), $basePath);
        $items[$item->relPath] = $item;
      }
      elseif ($candidate->isFile()) {
        $item = FileItem::create($candidate->getPathname(), $basePath);
        $items[$item->relPath] = $item;
      }
      elseif ($candidate->isLink()) {
        $item = LinkItem::create($candidate->getPathname(), $basePath);
        $items[$item->relPath] = $item;
      }
    }
    ksort($items);
    return new self(Path::makeRelative($path, $basePath), $items);
  }

}

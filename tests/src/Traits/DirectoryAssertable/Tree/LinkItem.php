<?php

declare(strict_types=1);
namespace Geeks4Change\Tests\GeekyDeploy\Traits\DirectoryAssertable\Tree;

use Symfony\Component\Filesystem\Path;

final class LinkItem implements FileTreeInterface {

  public function __construct(
    public readonly string $relPath,
    public readonly string $target,
  ) {}

  public static function create(string $path, string $basePath): self {
    is_link($path) || throw new \UnexpectedValueException("ust be link: $path");
    return new self(Path::makeRelative($path, $basePath), readlink($path));
  }

}

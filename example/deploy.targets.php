<?php declare(strict_types=1);

use Geeks4Change\GeekyDeploy\Builder\DrupalBuilder;
use Geeks4Change\GeekyDeploy\Builder\DrupalDefault;
use Geeks4Change\GeekyDeploy\Builder\DrupalSettings;
use Geeks4Change\GeekyDeploy\Server\Fsb\FsbServer;
use Geeks4Change\GeekyDeploy\Target\Local\LocalTarget;
use Geeks4Change\GeekyDeploy\Target\Remote\RemoteTarget;
use Geeks4Change\GeekyDeploy\Target\Targets;

function getDeployTargets(): Targets {
  $targets = Targets::create();

  $drushYamlData = DrupalDefault::DrushYaml();

  $settings = DrupalSettings::create(
    hashSalt: 'EWXytVaq8lEIGzG5PH5a4bKaqJSlbfmR6uNiILnKX/q1ka1Ztr1smQ'
  );

  $targets->addLocal(LocalTarget::create(
    name: 'local',
    port: 9999,
    builder: DrupalBuilder::create(
      settings: $settings
        ->withFilePrivatePath('../private/sites/default', '../private')
        ->withDatabase(DrupalDefault::SqliteDb()),
      drushYamlData: $drushYamlData,
    ),
  ));

  $targets->addRemote(RemoteTarget::create(
    name: 'deploytest1',
    url: 'https://deploytest1.greenopolis.net',
    server: FsbServer::create('s3495', 'c145s'),
    builder: DrupalBuilder::create(
      settings: $settings
        ->withConfigOverride('system.performance', 'css.preprocess', 1)
        ->withConfigOverride('system.performance', 'js.preprocess', 1)
        ->withTrustedHosts('deploytest1.greenopolis.net')
        ->withFilePrivatePath('../private/sites/default', '../private'),
      drushYamlData: $drushYamlData,
    ),
  ));

  return $targets->freeze();
}

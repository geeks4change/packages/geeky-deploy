<?php

// Get deployment targets from separate file, for convenience.
include __DIR__ . '/deploy.targets.php';
// Include 'build' and 'deploy' commands.
// Needs: `castor composer require geeks4change/geeky-deploy:^6`
\Castor\import('composer://geeks4change/geeky-deploy', 'castor-includes/deploy.castor.php');
